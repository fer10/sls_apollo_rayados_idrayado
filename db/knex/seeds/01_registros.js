
const tableName = 'registros';

exports.seed = function (knex) {
  return knex(tableName).del()
    .then(function () {
      return knex(tableName).insert([
        {
          id: 100110,
          uuid: 'f7fe08ba-c792-4dcd-afd6-42a4fc34f784',
          email: 'fer@fernando.com.mx',
          fecha_create: '2018-03-25 17:10:22',
          last_login: '2023-11-13 12:40:27',
          nombre: 'F',
          segundo_nombre: '',
          apellido_paterno: 'T',
          apellido_materno: 'T',
          genero: 'Masculino',
          fecha_nacimiento: '2011-01-01',
          tel: '0000000000',
          cell: '0000000000',
          email_url_callback: 'http://www.domain.com/password/',
          password: '$5$rounds=535000$yOT6vNJndJsaA3iw$0uuoFln4Bg7Y2lAsYyL1p.Nq72ZIsfRdyGzwaWe3H3D',
          email_confirm: 1
        },
        {
          id: 136878,
          uuid: '82427fc4-8502-42b8-8445-fb2bb407f786',
          email: 'randomtestdata@mailinator.com',
          fecha_create: '2019-01-23 12:41:54',
          last_login: '2023-11-16 08:01:48',
          nombre: 'Name',
          segundo_nombre: 'Name 2',
          apellido_paterno: 'Last name',
          apellido_materno: 'Last name 2',
          genero: 'Femenino',
          fecha_nacimiento: '2020-10-10',
          tel: '0000000000',
          cell: '0000000000',
          email_url_callback: 'http://abonos.rayados.com/registros/recupear_pass_code',
          password: '$5$rounds=535000$ASlP8YPUsKu5PiTX$9fwAuD9ymBiOhDlD.bKKTSfU2xkqOWO2wk9I8.YNW30',
          email_confirm: 1
        }

      ]);
    });
};
