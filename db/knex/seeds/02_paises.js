
const tableName = 'countries';

exports.seed = function (knex) {
  return knex(tableName).del()
    .then(function () {
      return knex(tableName).insert([
        {
          iso2: 'MX',
          iso3: 'MEX',
          name: 'México'
        },
        {
          iso2: 'US',
          iso3: 'USA',
          name: 'United State'
        }
      ]);
    });
};
