const tableName = 'registros';
const tableDefaultrCollate = 'utf8_general_ci';
exports.up = function (knex) {
  return knex.schema
    .createTable(tableName, function (table) {
      table.increments('id').primary();
      table.string('uuid', 45).unique().notNullable();

      table.string('email', 250).unique();

      table.string('nombre', 100);
      table.string('segundo_nombre', 100);
      table.string('apellido_paterno', 50);
      table.string('apellido_materno', 50);

      table.string('genero', 45);
      table.date('fecha_nacimiento');

      table.string('tel', 45);
      table.string('cell', 45);

      table.string('street', 250);
      table.string('number', 45);
      table.string('number_in', 45);

      table.string('postal_code', 20);
      table.string('suburb', 200);
      table.string('town', 200);
      table.string('state', 200);
      table.string('city', 70);
      table.string('country', 200);

      table.string('country_iso2', 45)
        .references('iso2').inTable('countries').withKeyName('r_iso2_c_iso2_foreign');

      table.string('email_url_callback', 150);
      table.string('password', 250);

      table.boolean('email_confirm', 1);
      table.boolean('email_auto_verify', 1);

      table.string('openpay_id', 150);
      table.string('notify_email', 250);

      table.string('origin', 150);
      table.datetime('last_login');

      table.datetime('fecha_create');
      table.timestamps();
      table.datetime('deleted_at');

      table.collate(tableDefaultrCollate);
    });
};

exports.down = function (knex) {
  return knex.schema
    .dropTable(tableName);
};
