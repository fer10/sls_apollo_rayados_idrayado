const tableName = 'address_sends';
const tableDefaultrCollate = 'utf8_general_ci';
exports.up = function (knex) {
  return knex.schema
    .createTable(tableName, function (table) {
      table.string('uuid', 45).primary().unique().notNullable();
      table.string('user_uuid', 45).notNullable()
        .references('uuid').inTable('registros');

      table.string('alias', 50);

      table.string('street', 250);
      table.string('number', 45);
      table.string('number_in', 45);

      table.string('postal_code', 20);
      table.string('suburb', 200); // colonia
      table.string('town', 200);
      table.string('state', 200);
      table.string('city', 70);
      table.string('country', 200);

      table.string('country_iso2', 45).notNullable()
        .references('iso2').inTable('countries').withKeyName('as_iso2_c_iso2_foreign');

      table.string('person_name', 45);
      table.string('tel', 45);

      table.timestamps();
      table.datetime('deleted_at');

      table.collate(tableDefaultrCollate);
    });
};

exports.down = function (knex) {
  return knex.schema
    .dropTable(tableName);
};
