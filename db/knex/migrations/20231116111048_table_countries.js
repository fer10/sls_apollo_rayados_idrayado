const tableName = 'countries';
const tableDefaultrCollate = 'utf8_general_ci';
exports.up = function (knex) {
  return knex.schema
    .createTable(tableName, function (table) {
      table.string('iso2', 45).unique().primary().notNullable();
      table.string('iso3', 45).unique();
      table.string('name', 45);

      table.timestamps();
      table.datetime('deleted_at');

      table.collate(tableDefaultrCollate);
    });
};

exports.down = function (knex) {
  return knex.schema
    .dropTable(tableName);
};
