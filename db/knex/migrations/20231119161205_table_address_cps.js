const tableName = 'address_cps';
const tableDefaultrCollate = 'utf8_general_ci';
exports.up = function (knex) {
  return knex.schema.hasTable(tableName).then(function (exists) {
    if (!exists) {
      return knex.schema.createTable(tableName, function (table) {
        table.increments('id').primary();

        table.string('codigo', 250);
        table.string('asenta', 250);
        table.string('asenta_tipo', 250);
        table.string('municipio', 250);
        table.string('estado', 250);
        table.string('ciudad', 250);
        table.string('country_iso2', 45).notNullable()
          .references('iso2').inTable('countries').withKeyName('ac_iso2_c_iso2_foreign');

        table.timestamps();
        table.datetime('deleted_at');

        table.index('codigo');
        table.collate(tableDefaultrCollate);
      });
    }
  });
};

exports.down = function (knex) {
  /*
  return knex.schema
    .dropTable(tableName);
    */
};
