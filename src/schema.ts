import { makeExecutableSchema } from 'apollo-server-lambda';

import includeTypeDefs from './modules/includesTypeDefs';

import * as SacalarDate from './scalars/date';
import * as SacalarDateTime from './scalars/dateTime';

import * as preSingedUrlGet from './directives/preSingedUrlGet';
import * as recaptcha from './directives/recaptcha';

import helloWorldTypeDefs from './modules/helloWorld/schema';
import helloWorldResolvers from './modules/helloWorld/resolver';

import userTypeDefs from './modules/Users/schema';
import userResolvers from './modules/Users/resolver';

import seassionsTypeDefs from './modules/Seassions/schema';
import seassionsResolvers from './modules/Seassions/resolver';

import AddressSendsTypeDefs from './modules/AddressSends/schema';
import AddressSendsResolvers from './modules/AddressSends/resolver';

import addressPCTypeDefs from './modules/AddressPC/schema';
import addressPCResolvers from './modules/AddressPC/resolver';

import FileUploadDefs from './modules/FileUpload/schema';
import FileUploadResolvers from './modules/FileUpload/resolver';

const schema = makeExecutableSchema({
  typeDefs: [
    SacalarDate.shema, SacalarDateTime.shema, preSingedUrlGet.shema,
    includeTypeDefs, helloWorldTypeDefs, userTypeDefs, seassionsTypeDefs, addressPCTypeDefs,
    AddressSendsTypeDefs, FileUploadDefs
  ],
  resolvers: [
    SacalarDate.resolver, SacalarDateTime.resolver,
    helloWorldResolvers, userResolvers, seassionsResolvers, addressPCResolvers,
    AddressSendsResolvers, FileUploadResolvers
  ],
  schemaDirectives: {
    // @ts-ignore
    preSingedUrlGet: preSingedUrlGet.ShemaDirective
  }
});

export default schema;
