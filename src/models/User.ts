import { ApolloError } from 'apollo-server-lambda';
import { S3, DynamoDB } from 'aws-sdk';

import { v4 as uuidv4 } from 'uuid';
import * as jwt from 'jsonwebtoken';

import BaseModel from './BaseModel';
import * as crypto from 'crypto';

const fetch = require('node-fetch');
const Moment = require('moment-timezone').tz.setDefault('America/Mexico_City');
const dynamoClient = new DynamoDB.DocumentClient();
const DYNAMO_TABLE_NAME = 'idRayado';

const ENCRYPTION_KEY = process.env.ENCRYPTION_KEY;
const IV_LENGTH = 16;

const s3 = new S3();
const s3BucketName = process.env.BUCKET_S3;
const s3FolderName = 'registros/data';

interface IdoSingUp{
 email:string,
 password:string,
 origin:string,
 email_auto_verify:boolean,
 callback_url?:string
}

interface IreSendConfirmCode{
  email:string,
  email_auto_verify:boolean,
  callback_url?:string
}

export default class User extends BaseModel {
     public id!: number
     public uuid!: string
     public email: string

     public street?: string
     public number_int?: string
     public number_ext?: string

     public suburb!: string
     public town!: string
     public state!: string
     public city!: string
     public postal_code!: string

     public photo_url: string
     public photo_key: string

     public country_iso2!: string

     public origin?:string

     static tableName = 'registros';
     private static expiredTime='12h';

     static get idColumn () {
       return 'uuid';
     }

     // Colum Mapping es/en
     private static getRenameAndDelete = ({ object, name, rename }:{object:object, name:string, rename:string}) => {
       if (object[name] !== undefined) {
         object[rename] = object[name];
         delete object[name];
       }
       return object;
     };

     static columnNameMappers = {
       parse (obj) {
         return {
           ...obj,
           name: obj.nombre,
           second_name: obj.segundo_nombre,
           last_name: obj.apellido_paterno,
           second_last_name: obj.apellido_materno,
           gender: obj.genero,
           birth_date: obj.fecha_nacimiento
         };
       },
       format (object) {
         object = User.getRenameAndDelete({ object, name: 'name', rename: 'nombre' });
         object = User.getRenameAndDelete({ object, name: 'second_name', rename: 'segundo_nombre' });
         object = User.getRenameAndDelete({ object, name: 'last_name', rename: 'apellido_paterno' });
         object = User.getRenameAndDelete({ object, name: 'second_last_name', rename: 'apellido_materno' });
         object = User.getRenameAndDelete({ object, name: 'gender', rename: 'genero' });
         object = User.getRenameAndDelete({ object, name: 'birth_date', rename: 'fecha_nacimiento' });
         return object;
       }
     };
     /// End Colum Mapping es/en

     async $beforeUpdate (opt:any, queryContext:any) {
       if (opt.patch) {
         await this.updateFiles(opt);
       }

       await super.$beforeUpdate(opt, queryContext);
     }

     public static async find_valid_user_trow (user_uuid:string) {
       const userData = await User.query().findById(user_uuid);
       if (!userData) throw new ApolloError('User not found.', 'USER_NOT_FOUND');

       return userData;
     }

     public static async find_list_seassions (user_uuid:string) {
       const responseListSeassions = await dynamoClient.query({
         TableName: DYNAMO_TABLE_NAME,
         KeyConditionExpression: '#pk = :pk and begins_with(#sk,:sk)',
         ExpressionAttributeNames: {
           '#pk': 'PK',
           '#sk': 'SK'
         },
         ExpressionAttributeValues: {
           ':pk': 'SESSION',
           ':sk': `USER#${user_uuid}#SESSION#`
         }
       }).promise();

       return responseListSeassions;
     }

     public static async find_seassion (user_uuid:string, seassion_uuid:string) {
       const responseListSeassions = await dynamoClient.query({
         TableName: DYNAMO_TABLE_NAME,
         KeyConditionExpression: '#pk = :pk and begins_with(#sk,:sk)',
         ExpressionAttributeNames: {
           '#pk': 'PK',
           '#sk': 'SK'
         },
         ExpressionAttributeValues: {
           ':pk': 'SESSION',
           ':sk': `USER#${user_uuid}#SESSION#${seassion_uuid}`
         }
       }).promise();

       return responseListSeassions;
     }

     public static async delete_seassion (user_uuid:string, seassion_uuid:string) {
       try {
         const responseListSeassions = await dynamoClient.delete({
           TableName: DYNAMO_TABLE_NAME,
           ConditionExpression: 'attribute_exists(SK)',
           Key: {
             PK: 'SESSION',
             SK: `USER#${user_uuid}#SESSION#${seassion_uuid}`
           }
         }).promise();

         return true;
       } catch (e) {
         return false;
       }
     }

     public static async createAccesTokenTest () {
       if (!process.env.IS_OFFLINE) return '';

       return jwt.sign({
         uuid: '',
         session_uuid: null,
         type: 'access'
       }, process.env.JWT_SECRET, { expiresIn: User.expiredTime });
     }

     public static async generateAccessToken (session_token:string) {
       const arraysessionToken = session_token.split('.');
       const dataSessionTOken = arraysessionToken.length > 1 ? arraysessionToken[1] : arraysessionToken[0];

       const decryptData:{uuid:string, session_uuid:string, type:string} = JSON.parse(User.decrypt(dataSessionTOken));

       const responseSession = await dynamoClient.get({
         TableName: DYNAMO_TABLE_NAME,
         Key: {
           PK: 'SESSION',
           SK: `USER#${decryptData.uuid}#SESSION#${decryptData.session_uuid}`
         }
       }).promise();

       if (!responseSession.Item) throw new ApolloError('session not found.', 'SESSION_NOT_FOUND');

       return jwt.sign({
         uuid: decryptData.uuid,
         session_uuid: decryptData.session_uuid,
         type: 'access'
       }, process.env.JWT_SECRET, { expiresIn: User.expiredTime });
     }

     public static async generateAccessTempToken ({ uuid }:{uuid:string}) {
       return jwt.sign({
         uuid: uuid,
         session_uuid: null,
         type: 'accessTemp'
       }, process.env.JWT_SECRET, { expiresIn: User.expiredTime });
     }

     protected static generateSessionToken (user_uuid:string, seassion_uuid:string) {
       const data = {
         uuid: user_uuid,
         session_uuid: seassion_uuid,
         type: 'session'
       };

       return seassion_uuid + '.' + User.encrypt(JSON.stringify(data));
     }

     public static async getUserInfo ({ uuid }:{uuid:string}) {
       const filePath = `${s3FolderName}/${uuid}.json`;

       // Valida si existe
       try {
         const fileObject = await s3.getObject({
           Bucket: s3BucketName,
           Key: filePath
         }).promise();

         const InfoJson = JSON.parse((fileObject.Body) ? fileObject.Body.toString('utf-8') : '{}');
         return {
           ...InfoJson,
           name: InfoJson.nombre,
           second_name: InfoJson.segundo_nombre,
           last_name: InfoJson.apellido_paterno,
           second_last_name: InfoJson.apellido_materno,
           gender: InfoJson.genero,
           birth_date: InfoJson.fecha_nacimiento
         };
       } catch (e) {
         const infoUser = await this.find_valid_user_trow(uuid);

         try {
           // Se  guarda la respuesta
           await s3.putObject({
             Bucket: s3BucketName,
             Key: filePath,
             ContentType: 'application/json',
             Body: JSON.stringify(infoUser)
           }).promise();
         } catch (e) {}

         return infoUser;
       }
     }

     public static async deleteUserInfo ({ uuid }:{uuid:string}) {
       const filePath = `${s3FolderName}/${uuid}.json`;
       try {
         // Se borra el archivo
         await s3.deleteObject({
           Bucket: s3BucketName,
           Key: filePath
         }).promise();
       } catch (e) {}
     }

     public static getInfoAccesToken (access_token:string):{uuid:string, session_uuid:string, iat:number, exp:number} {
       try {
         return jwt.verify(access_token, process.env.JWT_SECRET);
       } catch (e) {
         switch (e.message) {
           case 'invalid token':
             throw new ApolloError('User JsonWbToken is invalid.', 'TOKEN_INVALID');
             break;
           case 'jwt malformed':
             throw new ApolloError('User JsonWbToken malformed.', 'TOKEN_MALFORMED');
             break;
           case 'jwt expired':
             throw new ApolloError('User token is expired.', 'TOKEN_EXPIRED');
             break;
           case 'invalid signature':
             throw new ApolloError('User JsonWbToken is invalid signature.', 'TOKEN_INVALID');
             break;
           default:
             throw e;
             break;
         }
       }
     }

     private static async doSingUp ({ email, password, origin, email_auto_verify, callback_url }:IdoSingUp) {
       const response = await fetch(`${process.env.ENDPOINT_LOGIN}/profiles-v1/sign_up`, {
         method: 'post',
         headers: {
           'Content-Type': 'application/json',
           'x-api-key': process.env.APIKEY
         },
         body: JSON.stringify({
           email: email,
           password: password,
           email_url_callback: callback_url,
           email_auto_verify
         })
       });
       return await response.json();
     }

     private static async doResendConfirmcode ({ email, callback_url, email_auto_verify }:IreSendConfirmCode) {
       const response = await fetch(`${process.env.ENDPOINT_LOGIN}/profiles-v1/resend_confirmation_code`, {
         method: 'post',
         headers: {
           'Content-Type': 'application/json',
           'x-api-key': process.env.APIKEY
         },
         body: JSON.stringify({
           email: email,
           email_url_callback: callback_url,
           email_auto_verify
         })
       });
       return await response.json();
     }

     public static async createUser ({ email, password, origin, email_auto_verify, callback_url }:IdoSingUp) {
       const responseJson = await this.doSingUp({ email, password, origin, email_auto_verify, callback_url });
       if (responseJson.error) this.trowError(responseJson.error);

       // Se guarda la tag de creacion
       if (origin) await this.query().patchAndFetchById(responseJson.uuid, { origin });

       return responseJson;
     }

     public static async reSendConfirmCode ({ email, callback_url, email_auto_verify }:IreSendConfirmCode) {
       const responseJson = await this.doResendConfirmcode({ email, callback_url, email_auto_verify });
       if (responseJson.error) {
         if (responseJson.error.type === 'InvalidParameterException') responseJson.error.type = 'UserAlreadyConfirmed';

         this.trowError(responseJson.error);
       }

       return responseJson;
     }

     public static async createUserAndReSendEmail (
       { email, password, origin, email_auto_verify, callback_url }:
      {email:string, password:string, tag:string, origin:string, email_auto_verify:boolean, callback_url?:string}
     ) {
       const responseJson = await this.doSingUp({ email, password, origin, email_auto_verify, callback_url });
       if (responseJson.error) {
         if (responseJson.error?.type === 'UsernameExistsException') {
           // When user exist
           const responseJsonConfirm = await this.doResendConfirmcode({ email, callback_url, email_auto_verify });
           if (responseJsonConfirm.error) {
             if (responseJsonConfirm.error.type === 'InvalidParameterException') responseJsonConfirm.error.type = 'UserAlreadyConfirmed';

             this.trowError(responseJsonConfirm.error);
           }
           return responseJsonConfirm;
         } else this.trowError(responseJson.error);
       }

       // Se guarda la tag de creacion
       if (origin) await this.query().patchAndFetchById(responseJson.uuid, { origin });

       return responseJson;
     }

     public static async confirmEmailAccount ({ user_uuid, code }:{user_uuid:string, code:string}) {
       const response = await fetch(`${process.env.ENDPOINT_LOGIN}/profiles-v1/confirm_sign_up`, {
         method: 'post',
         headers: {
           'Content-Type': 'application/json',
           'x-api-key': process.env.APIKEY
         },
         body: JSON.stringify({
           uuid: user_uuid,
           code: code,
           email: ''
         })
       });
       const responseJson = await response.json();

       if (responseJson.error) {
         if (responseJson.error.type === 'NotAuthorizedException') responseJson.error.type = 'UserEmailConfirmed';

         this.trowError(responseJson.error);
       }
       return responseJson;
     }

     public static async passwordRecovery ({ email, callback_url }:{email:string, callback_url?:string}) {
       const response = await fetch(`${process.env.ENDPOINT_LOGIN}/profiles-v1/forgot_password`, {
         method: 'post',
         headers: {
           'Content-Type': 'application/json',
           'x-api-key': process.env.APIKEY
         },
         body: JSON.stringify({
           email: email,
           email_url_callback: callback_url
         })
       });
       const responseJson = await response.json();

       if (responseJson.error) {
         if (responseJson.error.type === 'InvalidParameterException') responseJson.error.type = 'UserNotConfirmedException';

         this.trowError(responseJson.error);
       }
       return responseJson;
     }

     public static async passwordRecoveryCode ({ user_uuid, code, new_password }:{user_uuid:string, code:string, new_password:string}) {
       const response = await fetch(`${process.env.ENDPOINT_LOGIN}/profiles-v1/confirm_forgot_password`, {
         method: 'post',
         headers: {
           'Content-Type': 'application/json',
           'x-api-key': process.env.APIKEY
         },
         body: JSON.stringify({
           uuid: user_uuid,
           code: code,
           new_password: new_password
         })
       });
       const responseJson = await response.json();
       if (responseJson.error) this.trowError(responseJson.error);

       return responseJson;
     }

     public static async doLoginIn ({ email, password, device, origin, email_auto_verify, access_temp } :{email:string, password:string, device:object, origin:string, email_auto_verify:boolean, access_temp:boolean}) {
       const response = await fetch(`${process.env.ENDPOINT_LOGIN}/profiles-v1/login_in`, {
         method: 'post',
         headers: {
           'Content-Type': 'application/json',
           'x-api-key': process.env.APIKEY
         },
         body: JSON.stringify({
           email: email,
           password: password
         })
       });
       const responseJson = await response.json();

       if (responseJson.error) this.trowError(responseJson.error);

       if (access_temp) {
         return {
           session_token: null,
           access_token: this.generateAccessTempToken({ uuid: responseJson.uuid })
         };
       }

       const user_uuid = responseJson.uuid;
       const session_token = await this.createSession({ user_uuid, device, origin });
       return {
         session_token: session_token,
         access_token: this.generateAccessToken(session_token)
       };
     }

     public static async createSession ({ user_uuid, device, origin, session_parent }:{user_uuid:string, device:object, origin:string, session_parent?:string}) {
       const seassionData = {
         uuid: uuidv4(),
         user_uuid: user_uuid,
         session_parent: session_parent,
         last_activity: Moment().format(),
         create_at: Moment().format(),
         device: device,
         origin: origin
       };

       await dynamoClient.put({
         TableName: DYNAMO_TABLE_NAME,
         Item: {
           PK: 'SESSION',
           SK: `USER#${user_uuid}#SESSION#${seassionData.uuid}`,
           ...seassionData
         }
       }).promise();

       console.log('CREATE SESSION:', seassionData);

       return this.generateSessionToken(user_uuid, seassionData.uuid);
     }

     private static trowError (error) {
       switch (error.type) {
         case 'UserNotFoundException':
           throw new ApolloError('User not found.', 'USER_NOT_FOUND');
           break;
         case 'UserNotConfirmedException':
           throw new ApolloError('User email not confirmed.', 'USER_NOT_CONFIRMED');
           break;
         case 'NotAuthorizedException':
           throw new ApolloError('User not authorized.', 'USER_NOT_AUTHORIZED');
           break;
         case 'LimitExceededException':
           throw new ApolloError('User limit exceeded.', 'USER_LIMIT_EXCEEDED');
           break;
         case 'UsernameExistsException':
           throw new ApolloError('User already exist.', 'USER_ALREADY_EXIST');
           break;
         case 'UserAlreadyConfirmed':
           throw new ApolloError('User already confirmed.', 'USER_ALREADY_CONFIRMED');
           break;
         case 'RecordNotFound':
           throw new ApolloError('User not found.', 'USER_NOT_FOUND');
           break;
         case 'CodeMismatchException':
           throw new ApolloError('Invalid code.', 'INVALID_CODE');
           break;
         case 'UserEmailConfirmed':
           throw new ApolloError('Email already confirmed.', 'EMAIL_ALREADY_CONFIRMED');
           break;
         default:
           throw new ApolloError(error.message, error.type);
           break;
       }
     }

     protected static encrypt (text) {
       const iv = crypto.randomBytes(IV_LENGTH);
       const cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv);
       let encrypted = cipher.update(text);

       encrypted = Buffer.concat([encrypted, cipher.final()]);

       return iv.toString('hex') + ':' + encrypted.toString('hex');
     }

     protected static decrypt (text) {
       const textParts = text.split(':');
       const iv = Buffer.from(textParts.shift(), 'hex');
       const encryptedText = Buffer.from(textParts.join(':'), 'hex');
       const decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv);
       let decrypted = decipher.update(encryptedText);

       decrypted = Buffer.concat([decrypted, decipher.final()]);

       return decrypted.toString();
     }

     private async updateFiles (opt:any) {
       this.photo_url = this.photo_key ? await this.uploadFile({ keySource: this.photo_key, keyDest: 'registros/photos', opt }) : this.photo_url;
       delete this.photo_key;
     }

     private async uploadFile ({ keySource, keyDest, opt }:{keySource:string, keyDest:string, opt:any }) {
       const keyDestPath = `${keyDest}/${opt?.old.uuid}_${this.getFilename(keySource)}`;
       await this.moveUploadFile({
         key_origin: keySource,
         key_dest: keyDestPath
       });

       return keyDestPath;
     }

     private async moveUploadFile ({ key_origin, key_dest }:{key_origin:string, key_dest:string}) {
       await s3.copyObject({
         Bucket: s3BucketName,
         CopySource: `${s3BucketName}/${key_origin}`,
         Key: key_dest
         // ACL:"public-read",
       }).promise();

       return key_dest;
     }

     private getFilename (path:string) {
       return path.substring(path.lastIndexOf('/') + 1);
     }

     $beforeValidate (jsonSchema, json, opt) {
       console.log('Json validate:');

       // Validation names en and es
       if (json.name !== null) {
         jsonSchema.required = [...jsonSchema.required, 'name', 'last_name', 'gender', 'birth_date'];
         jsonSchema.properties = {
           ...jsonSchema.properties,
           name: { type: 'string', minLength: 1, maxLength: 255 },
           second_name: { type: 'string', minLength: 0, maxLength: 255 },
           last_name: { type: 'string', minLength: 1, maxLength: 255 },
           second_last_name: { type: 'string', minLength: 0, maxLength: 255 },
           gender: { type: 'string', enum: ['Masculino', 'Femenino'] },
           birth_date: { type: 'string', format: 'date' }
         };
       } else {
         jsonSchema.required = [...jsonSchema.required, 'nombre', 'apellido_paterno', 'genero', 'fecha_nacimiento'];
         jsonSchema.properties = {
           ...jsonSchema.properties,
           nombre: { type: 'string', minLength: 1, maxLength: 255 },
           segundo_nombre: { type: 'string', minLength: 0, maxLength: 255 },
           apellido_paterno: { type: 'string', minLength: 1, maxLength: 255 },
           apellido_materno: { type: 'string', minLength: 0, maxLength: 255 },
           genero: { type: 'string', enum: ['Masculino', 'Femenino'] },
           fecha_nacimiento: { type: 'string', format: 'date' }
         };
       }

       /*
       // Validations of countries
       if (json.country_iso2) {
         jsonSchema.required = [...jsonSchema.required, 'postal_code', 'state', 'city', 'town'];
         jsonSchema.properties = {
           ...jsonSchema.properties,
           postal_code: { type: 'string', minLength: 1, maxLength: 255 },
           town: { type: 'string', minLength: 1, maxLength: 255 },
           state: { type: 'string', minLength: 1, maxLength: 255 },
           city: { type: 'string', minLength: 1, maxLength: 255 }
         };
         if (json.country_iso2 === 'MX') {
           jsonSchema.required = [...jsonSchema.required, 'suburb'];
           jsonSchema.properties = {
             ...jsonSchema.properties,
             suburb: { type: 'string', minLength: 1, maxLength: 255 }
           };
         }
       }
       */

       return jsonSchema;
     }

     static get jsonSchema () {
       return {
         type: 'object',
         required: [],
         properties: {
           id: { type: 'integer' },
           uuid: { type: 'string' },
           email: { type: 'string', format: 'email' },

           cell: {
             type: 'string',
             minLength: 10,
             maxLength: 10
           },

           street: { type: 'string' },
           number: { type: 'string' },
           number_in: { type: 'string' },

           country_iso2: { type: 'string', enum: ['MX', 'US', null] },
           notify_email: {
             type: 'string',
             oneOf: [
               { enum: [''] },
               { format: 'email' }
             ]
           }
         }
       };
     }
};
