import { ApolloError } from 'apollo-server-lambda';
import { S3 } from 'aws-sdk';

import BaseModel from './BaseModel';

const s3 = new S3();
const s3BucketName = process.env.BUCKET_S3;
const s3FolderName = 'AddressPC';

export default class AddressPC extends BaseModel {
     public id!: number

     public codigo!: string
     public asenta!: string
     public asenta_tipo!: string
     public municipio!: string
     public estado!: string
     public ciudad!: string
     public country_iso2:string

     static tableName = 'address_cps';

     static get idColumn () {
       return 'id';
     }

     public static async getPCInfo ({ pc = '', country_iso2 }:{pc:string, country_iso2?:string}) {
       country_iso2 = country_iso2?.toLocaleUpperCase();
       const filePath = `${s3FolderName}/${country_iso2}/${pc}.json`;

       // Valida si existe
       try {
         const fileObject = await s3.getObject({
           Bucket: s3BucketName,
           Key: filePath
         }).promise();

         return JSON.parse((fileObject.Body) ? fileObject.Body.toString('utf-8') : '{}');
       } catch (e) {
         const data = await this.getPCInfoDB({ pc, country_iso2 });

         try {
           // Se  guarda la respuesta
           await s3.putObject({
             Bucket: s3BucketName,
             Key: filePath,
             ContentType: 'application/json',
             Body: JSON.stringify(data)
           }).promise();
         } catch (e) {}

         return data;
       }
     }

     private static async getPCInfoDB ({ pc, country_iso2 }:{pc:string, country_iso2:string}) {
       const listItems = await this.query().where({ codigo: pc, country_iso2 });
       if (listItems.length === 0) throw new ApolloError('postal code not found.', 'POSTAL_CODE_NOT_FOUND');

       return {
         pc: pc,
         suburbs: country_iso2 === 'MX' ? listItems.map(item => item.asenta) : null,
         suburbs_type: listItems[0].asenta_tipo,
         town: listItems[0].municipio,
         state: listItems[0].estado,
         city: listItems[0].ciudad,
         country: country_iso2 === 'MX' ? 'México' : 'United State'
       };
     }
};
