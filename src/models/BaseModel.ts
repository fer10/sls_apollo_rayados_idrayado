import { Model } from 'objection';
const moment = require('moment-timezone').tz.setDefault('America/Mexico_City');

export default class BaseModel extends Model {
     public created_at?:string
     public updated_at?:string

     public $beforeInsert (queryContext:any = null) {
       this.created_at = moment().format('YYYY-MM-DD HH:mm:ss');
     }

     public $beforeUpdate (opt:any, queryContext:any) {
       this.updated_at = moment().format('YYYY-MM-DD HH:mm:ss');
     }
};
