import { ApolloError } from 'apollo-server-lambda';
import { v4 as uuidv4 } from 'uuid';

import BaseModel from './BaseModel';
import AddressPC from './AddressPC';

const Moment = require('moment-timezone').tz.setDefault('America/Mexico_City');

interface IAddressValid{
  postal_code: string

  suburb?: string
  town?:string
  state: string
  city: string

  country?: string
  country_iso2?: string
}

export default class AddressSend extends BaseModel {
     public uuid!: string
     public user_uuid!: string

     public alias!: string
     public street?: string
     public number_int?: string
     public number_ext?: string

     public suburb!: string
     public town!: string
     public state!: string
     public city!: string
     public postal_code!: string

     public country_iso2!: string

     public person_name!: string
     public tel?: string

     public created_at:string
     public updated_at:string
     public deleted_at:string

     static tableName = 'address_sends';

     static get idColumn () {
       return 'uuid';
     }

     async $beforeInsert () {
       this.uuid = uuidv4();

       super.$beforeInsert();
     }

     public static async findByIdThrow (uuid:string) {
       const Item = await this.query().findById(uuid);
       if (!Item) throw new ApolloError('Address send not found.', 'ADDRESS_SEND_NOT_FOUND');
       return Item;
     }

     public static async findAccesByIdThrow (uuid:string, userUuid:string) {
       const Item = await this.findByIdThrow(uuid);

       if (Item.user_uuid !== userUuid) throw new ApolloError('User acces address_send deny.', 'ACCES_ADDRESS_SEND_DENY');
       if (Item.deleted_at !== null) throw new ApolloError('Address send not found.', 'ADDRESS_SEND_NOT_FOUND');

       return Item;
     }

     // Soft deleted
     static async beforeDelete ({ asFindQuery, cancelQuery }:any) {
       const [numAffectedItems] = await asFindQuery().patch({ deleted_at: Moment().format('YYYY-MM-DD HH:mm:ss') });

       cancelQuery(numAffectedItems);
     }

     public static async validInfoAdressThrow (params:IAddressValid) {
       const countryIso = params.country_iso2?.toLocaleUpperCase();
       if (countryIso) {
         const infoCountry = await AddressPC.getPCInfo({ pc: params.postal_code, country_iso2: countryIso });

         if (params.city !== infoCountry.city) throw new ApolloError('Address city are incorrect.', 'ADDRESS_CITY_INCORRECT');
         if (params.state !== infoCountry.state) throw new ApolloError('Address state are incorrect.', 'ADDRESS_STATE_INCORRECT');

         if (countryIso === 'MX') {
           if (params.town !== infoCountry.town) throw new ApolloError('Address town are incorrect.', 'ADDRESS_TOWN_INCORRECT');
           if (params.suburb !== '') {
             if (!infoCountry.suburbs.find(item => item === params.suburb)) throw new ApolloError('Address suburbs not are in the list', 'ADDRESS_SUBURB_INCORRECT');
           }
         }

         params.country = infoCountry.country;
       }

       return params;
     }
};
