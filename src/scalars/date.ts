import { GraphQLScalarType, Kind } from 'graphql';
const moment = require('moment-timezone').tz.setDefault("America/Mexico_City")

export const shema=`
     scalar Date
`;

let Scalar= new GraphQLScalarType({
  name: 'Date',
  description: 'Date scalar type on format "000-00-00"',
  serialize(value) {
    return moment(value).format('YYYY-MM-DD');
  },
  parseValue(value) {
    return moment(value).format('YYYY-MM-DD');
  },
  parseLiteral(ast) {
    // @ts-ignore
    let value=ast.value;

    let date = moment(value);
    if(date.isValid())
        return value
    else 
        throw new Error('Date type must be "000-00-00"');
  },
});

export const resolver={
  Date:Scalar
};

