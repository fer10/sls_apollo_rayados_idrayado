import { GraphQLScalarType, Kind } from 'graphql';
const moment = require('moment-timezone').tz.setDefault("America/Mexico_City")

export const shema=`
     scalar DateTime
`;

let Scalar= new GraphQLScalarType({
  name: 'DateTime',
  description: 'DateTime scalar type on format "0000-00-00T00:00:00-00:00"',
  serialize(value) {
    return moment(value).format();
  },
  parseValue(value) {
    return moment(value).format('YYYY-MM-DD HH:mm:ss');
  },
  parseLiteral(ast) {
    // @ts-ignore
    let value=ast.value;

    let date = moment(value);
    if(date.isValid())
        return value
    else 
        throw new Error('Date type must be "000-00-00 00:00:00"');
  },
});

export const resolver={
  DateTime:Scalar
};

