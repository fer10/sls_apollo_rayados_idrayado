import { SchemaDirectiveVisitor, ApolloError } from 'apollo-server-lambda';
import { defaultFieldResolver } from 'graphql';
const fetch = require('node-fetch');

export const shema = `
     directive @recaptcha on FIELD_DEFINITION
`;

export class ShemaDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition (field:any) {
    const { resolve = defaultFieldResolver } = field;

    field.resolve = async function (...args:any) {
      const seassionInfoUserToken = args[2];
      const token = seassionInfoUserToken.headers['x-recapchatoken'];

      const url = `https://www.google.com/recaptcha/api/siteverify?secret=${process.env.RECAPTCHA_SECRET}&response=${token}`;

      if (!process.env.IS_OFFLINE) {
        const validateCaptchaResponse = await fetch(url);
        const validateCaptcha = await validateCaptchaResponse.text();
        // console.log("Recapcha response:",validateCaptcha);
        const validateCaptchaJson = JSON.parse(validateCaptcha);

        if (validateCaptchaJson.success !== true) { throw new ApolloError('Invalida recaptcha code.', 'RECAPTCHA_INVALID'); }
      }

      return await resolve.apply(this, args);
    };
  }
};
