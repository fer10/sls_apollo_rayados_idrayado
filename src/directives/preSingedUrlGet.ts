import { gql, SchemaDirectiveVisitor } from 'apollo-server-lambda';
import { defaultFieldResolver } from 'graphql';
import { S3 } from 'aws-sdk';

export const shema = gql`
     directive @preSingedUrlGet (bucket:String, expires:Int ) on FIELD_DEFINITION
`;

export class ShemaDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition (field:any) {
    const s3 = new S3();

    const { resolve = defaultFieldResolver } = field;
    const { bucket, expires } = this.args; // Directive params

    field.resolve = async function (...args:any) {
      let resultField:string = await resolve.apply(this, args);

      if (!resultField) { return null; }

      // Quitar el primer slash
      resultField = resultField.replace(/^\/+/, '');

      return s3.getSignedUrl('getObject', {
        Bucket: bucket,
        Key: resultField,
        Expires: 60 * expires
      });
    };
  }
};
