import { ApolloServer } from 'apollo-server-lambda';
import * as knex from 'knex';

import { Model } from 'objection';
import schema from './schema';

const server = new ApolloServer({
  schema,
  playground: !!((process.env.STAGE === 'dev' || process.env.STAGE === 'local')),
  introspection: !!((process.env.STAGE === 'dev' || process.env.STAGE === 'local')),
  debug: !!((process.env.STAGE === 'dev' || process.env.STAGE === 'local')),
  tracing: false,

  formatError: (error:any) => {
    return {
      code: error.extensions.code,
      ...error
    };
  },
  context: (data:any) => {
    data.context.headers = data.event.headers;
    return data.context;
  }
});

let connDatabase:knex|null = null;
exports.handler = (event:any, context:any, callback:any) => {
  context.callbackWaitsForEmptyEventLoop = false;

  if (connDatabase == null) {
    const connectData = JSON.parse(process.env.DATABASE);

    connDatabase = knex({
      client: 'mysql2',
      debug: true,
      connection: {
        ...connectData,
        dateStrings: true
      }
    });
    console.log('Nueva conexion');
  }

  Model.knex(connDatabase);

  server.createHandler({
    cors: {
      origin: '*',
      methods: 'GET,POST,OPTIONS',
      credentials: true,
      allowedHeaders: ['Age', 'X-RecapchaToken'],
      exposedHeaders: ['Age', 'X-RecapchaToken']
    }
  })(event, context, callback);
};
