import User from '../../models/User';

export default {
  Mutation: {
    createAccesTokenTest: async (_:never, args:{session_token:string}) => User.createAccesTokenTest(),
    createAccesToken: async (_:never, args:{session_token:string}) => await User.generateAccessToken(args.session_token),

    allSeassions: async (_:never, args:{access_token:string}) => {
      const infoAccesToken = await User.getInfoAccesToken(args.access_token);
      const listSeassions = await User.find_list_seassions(infoAccesToken.uuid);

      return {
        data: listSeassions.Items,
        result: listSeassions.Items,
        total_count: listSeassions.Count
      };
    },
    getSeassion: async (_:never, args:{access_token:string}) => {
      const infoAccesToken = await User.getInfoAccesToken(args.access_token);
      const currentSeassion = await User.find_seassion(infoAccesToken.uuid, infoAccesToken.session_uuid);

      if (currentSeassion.Items && currentSeassion.Items.length > 0) {
        return currentSeassion.Items[0];
      } else return null;
    },
    delSeassion: async (_:never, args:{access_token:string, uuid_seassion:string}) => {
      const infoAccesToken = await User.getInfoAccesToken(args.access_token);
      return await User.delete_seassion(infoAccesToken.uuid, args.uuid_seassion);
    },
    createSeassion: async (_:never, args:{access_token:string, origin:string, device:object }) => {
      const infoAccesToken = await User.getInfoAccesToken(args.access_token);
      const session_token = User.createSession({
        user_uuid: infoAccesToken.uuid,
        session_parent: infoAccesToken.session_uuid,
        device: args.device,
        origin: args.origin
      });

      return {
        session_token
      };
    },
    getInfoUserAccesToken: async (_:never, args:{access_token:string}) => await User.getInfoAccesToken(args.access_token)
  }
};
