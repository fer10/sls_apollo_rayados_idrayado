export default `#graphql

   type SeassionsDevice{
      name:String
      platfom:String
   }

   type Seassions{
      uuid:ID
      user_uuid:ID
      create_at:DateTime
      last_activity:DateTime
      device:SeassionsDevice
      ip:String
   }

   type SeassionsConnection{
      data: [Seassions]
      total_count:Int

      total:Int
      results: [Seassions]
   }


   type SeassionsAccesToken{
      uuid:String
      session_uuid:String
      iat:DateTime
      exp:DateTime
   }



   type SeassionsNew{
      session_token:String
   }

   extend type Mutation{
      createAccesTokenTest: String

      createAccesToken(session_token:String!): String
      
      allSeassions(access_token:String!): SeassionsConnection
      getSeassion(access_token:String!): Seassions
      delSeassion(access_token:String!  uuid_seassion:ID!):Boolean

      createSeassion(access_token:String! device:UserDevice! origin:String): SeassionsNew

      getInfoUserAccesToken(access_token:String!): SeassionsAccesToken
   }

`;
