import User from '../../models/User';
import AddressCP from '../../models/AddressPC';

export default {
  Mutation: {
    getAddressByPC: async (_:never, { access_token, pc, country_iso2 }:{access_token:string, pc:string, country_iso2:string}) => {
      const infoAccesToken = await User.getInfoAccesToken(access_token);

      return AddressCP.getPCInfo({ pc, country_iso2 });
    }

  }
};
