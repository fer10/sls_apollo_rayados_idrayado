export default `#graphql

   type AddressByPC{
      pc:String
      suburbs:[String]
      suburbs_type:String

      town:String
      state:String
      city:String
      country:String
   }


   extend type Mutation{
      getAddressByPC(access_token:String! pc:String! country_iso2:String!): AddressByPC
   }

`;
