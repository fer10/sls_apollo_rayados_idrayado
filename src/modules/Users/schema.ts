export default `#graphql

   enum UserGenero{
      FEMENINO,
      MASCULINO
   }

   type User{
      id:Int
      uuid:ID
      email:String
         
      name:String
      second_name:String
      last_name:String
      second_last_name:String
      gender:UserGenero
      birth_date:String

      photo_url:String @preSingedUrlGet(bucket:"${process.env.BUCKET_S3}", expires:10)

      tel:String
      cell:String

      street:String
      number:String
      number_in:String

      postal_code:String
      suburb:String
      town:String
      state:String
      city:String
      country:String
      country_iso2:String

      notify_email:String
      email_confirm:Boolean

      fecha_create:DateTime
      last_login:DateTime


      nombre:String @deprecated(reason: "Use name instead.")
      segundo_nombre:String @deprecated(reason: "Use second_name instead.")
      apellido_paterno:String @deprecated(reason: "Use last_name instead.")
      apellido_materno:String @deprecated(reason: "Use second_last_name instead.")
      genero:UserGenero @deprecated(reason: "Use gender instead.")
      fecha_nacimiento:Date @deprecated(reason: "Use birth_date instead.")
   }

   input UserUpdateInput{
      name:String
      second_name:String
      last_name:String
      second_last_name:String
      gender:UserGenero
      birth_date:String

      photo_key:String
      
      tel:String
      cell:String 
      
      street:String
      number:String
      number_in:String

      country:String
      town:String
      suburb:String

      state:String
      city:String
      postal_code:String

      country_iso2:String

      notify_email:String  
      
      
      nombre:String @deprecated(reason: "Use name instead.")
      segundo_nombre:String @deprecated(reason: "Use second_name instead.")
      apellido_paterno:String @deprecated(reason: "Use last_name instead.")
      apellido_materno:String @deprecated(reason: "Use second_last_name instead.")
      genero:UserGenero @deprecated(reason: "Use gender instead.")
      fecha_nacimiento:Date @deprecated(reason: "Use birth_date instead.")
   }

   type UserLoginToken{
      access_token:String
      session_token:String
   }

   enum UserDevicePlatfom{
      IOS,
      ANDROID,
      WEB,
      WHATSAPP
   }


   input UserDevice{
      name:String!
      platfom:UserDevicePlatfom!
      push_token:String
   }

   extend type Mutation{
      getUser(access_token:String!): User
      updateUser(access_token:String! input:UserUpdateInput):User
      loginIn(email:String! password:String! origin:String device:UserDevice!  access_temp:Boolean=false): UserLoginToken

      createUserAndReSendEmail(email:String! password:String! callback_url:String email_auto_verify:Boolean=true origin:String): User

      createUser(email:String! password:String! callback_url:String email_auto_verify:Boolean=true origin:String): User
      reSendEmailConfirmCodeUser(email:String! callback_url:String email_auto_verify:Boolean=false): User
      confirmEmailCodeUser(user_uuid:ID! code:String!): User

      recoveryPasswordUser(email:String! callback_url:String):User
      confirmPasswordCodeUser(user_uuid:ID! code:String! new_password:String!):User

      ifExistUser(user_uuid:ID!):Boolean
   }

`;
