
import User from '../../models/User';
import AddressSend from '../../models/AddressSend';

export default {
  UserGenero: {
    FEMENINO: 'Femenino',
    MASCULINO: 'Masculino'
  },
  Mutation: {
    getUser: async (_:never, args:{access_token:string}) => {
      const infoAccesToken = await User.getInfoAccesToken(args.access_token);
      return await User.getUserInfo({ uuid: infoAccesToken.uuid });
    },
    updateUser: async (_:never, args:{access_token:string, input:User}) => {
      const infoAccesToken = await User.getInfoAccesToken(args.access_token);

      const user = await User.find_valid_user_trow(infoAccesToken.uuid);
      const infoUser = await AddressSend.validInfoAdressThrow(args.input);

      await user.$query().patch(infoUser);
      await User.deleteUserInfo({ uuid: user.uuid });

      return await User.getUserInfo({ uuid: user.uuid }); ;
    },
    loginIn: async (_:never, args:{email:string, password:string, origin:string, device:object, access_temp:boolean}) => await User.doLoginIn(args),

    createUser: async (_:never, args:{email:string, password:string, origin:string, email_auto_verify:string, callback_url?:string}) => await User.createUser(args),
    createUserAndReSendEmail: async (_:never, args:{email:string, password:string, origin:string, email_auto_verify:string, callback_url?:string}) => await User.createUserAndReSendEmail(args),
    reSendEmailConfirmCodeUser: async (_:never, args:{email:string, email_auto_verify:boolean, callback_url?:string}) => await User.reSendConfirmCode(args),
    confirmEmailCodeUser: async (_:never, args:{user_uuid:string, code:string}) => await User.confirmEmailAccount(args),

    recoveryPasswordUser: async (_:never, args:{email:string, callback_url?:string}) => await User.passwordRecovery(args),
    confirmPasswordCodeUser: async (_:never, args:{user_uuid:string, code:string, new_password:string}) => await User.passwordRecoveryCode(args),
    ifExistUser: async (_:never, { user_uuid }:{user_uuid:string}) => {
      try {
        await User.getUserInfo({ uuid: user_uuid });
        return true;
      } catch (e) {
        return false;
      }
    }
  }
};
