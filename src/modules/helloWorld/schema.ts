export default `#graphql

   type Query { 
      helloWorld: String!
   }
   
   type Mutation{
      doHelloWorld:String!
   }

`