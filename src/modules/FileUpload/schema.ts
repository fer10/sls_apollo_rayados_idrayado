export default `#graphql

   type FileUpload {
      presignedUrl:String
      contentType:String
      key:String
   }

   input FileUploadInput {
     contentType:String !
     fileName:String !
   }

   extend type Mutation{
      createFileUpload(input:FileUploadInput!): FileUpload
   }

`;
