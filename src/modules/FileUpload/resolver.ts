import { S3, SharedIniFileCredentials } from 'aws-sdk';
import { v4 as uuidv4 } from 'uuid';

const moment = require('moment-timezone').tz.setDefault('America/Mexico_City');

const s3 = new S3();
const s3BucketName = process.env.BUCKET_S3;

const getPresingedUrl = async ({ contentType, fileName }:{contentType:string, fileName:string}) => {
  return await s3.getSignedUrl('putObject', {
    Bucket: s3BucketName,
    Key: fileName,
    ContentType: contentType,
    Expires: 5 * 60
    // ACL: 'public-read',
  });
};

const resolvers = {
  Mutation: {
    createFileUpload: async (_:never, args:{input:{contentType:string, fileName:string }}) => {
      const keyFile = `temp_uploads/${moment().tz('America/Mexico_City').format('Y-MM-DD')}/${uuidv4()}_${encodeURI(args.input.fileName)}`;

      const response = getPresingedUrl({
        contentType: args.input.contentType,
        fileName: keyFile
      });

      return {
        presignedUrl: response,
        contentType: args.input.contentType,
        key: keyFile
      };
    }
  }
};

export default resolvers;
