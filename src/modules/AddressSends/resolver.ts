import { ApolloError } from 'apollo-server-lambda';

import User from '../../models/User';
import AddressSend from '../../models/AddressSend';

interface IAddressSendFields{
  alias: string

  street?: string
  number?: string
  number_in?: string

  suburb?: string
  town?:string
  state: string
  city: string
  postal_code: string
  country?: string

  country_iso2: string

  person_name: string
  tel?: string
}

export default {
  Mutation: {
    getAddressSend: async (_:never, args:{access_token:string, uuid_address_send:string}) => {
      const infoAccesToken = await User.getInfoAccesToken(args.access_token);
      return AddressSend.findAccesByIdThrow(args.uuid_address_send, infoAccesToken.uuid);
    },
    listAddressSend: async (_:never, args:{access_token:string, page:number, first:number}) => {
      const infoAccesToken = await User.getInfoAccesToken(args.access_token);

      return AddressSend.query().where({ user_uuid: infoAccesToken.uuid, deleted_at: null }).page(args.page, args.first);
    },
    createAddressSend: async (_:never, args:{access_token:string, input:IAddressSendFields}) => {
      const infoAccesToken = await User.getInfoAccesToken(args.access_token);

      const infoAdress = await AddressSend.validInfoAdressThrow(args.input);

      const item = await AddressSend.query().insertAndFetch({
        ...infoAdress,
        user_uuid: infoAccesToken.uuid
      });

      return item;
    },
    updateAddressSend: async (_:never, args:{access_token:string, uuid_address_send:string, input:IAddressSendFields}) => {
      const infoAccesToken = await User.getInfoAccesToken(args.access_token);
      const item = await AddressSend.findAccesByIdThrow(args.uuid_address_send, infoAccesToken.uuid);

      const infoAdress = await AddressSend.validInfoAdressThrow(args.input);

      return item.$query().patchAndFetch(infoAdress);
    },
    delAddressSend: async (_:never, args:{access_token:string, uuid_address_send:string}) => {
      const infoAccesToken = await User.getInfoAccesToken(args.access_token);
      const item = await AddressSend.findAccesByIdThrow(args.uuid_address_send, infoAccesToken.uuid);

      return await AddressSend.query().deleteById(item.uuid);
    }
  }
};
