export default `#graphql


   type AddressSend{
      uuid:ID
      user_uuid:ID

      alias:String

      street:String
      number:String
      number_in:String

      postal_code:String
      suburb:String
      town:String
      state:String
      city:String
      country:String
      country_iso2:String
      
      person_name:String
      tel:String
      
      created_at:DateTime
      updated_at:DateTime
   }

   type AddressSendConnection{
      results: [AddressSend]
      total:Int
   }

   input createAddressSendInput{
      alias:String

      country:String
      street:String
      number:String
      number_in:String

      suburb:String
      town:String
      state:String
      city:String
      postal_code:String

      country_iso2:String
      
      person_name:String
      tel:String
   }

   input updateAddressSendInput{
      alias:String!

      street:String!
      number:String
      number_in:String

      country:String
      town:String
      suburb:String

      state:String!
      city:String!
      postal_code:String!
      
      country_iso2:String!
      
      person_name:String!
      tel:String!
   }


   extend type Mutation{
      getAddressSend(access_token:String!  uuid_address_send:ID!): AddressSend
      listAddressSend(access_token:String!, first:Int=10, page:Int=0): AddressSendConnection
      
      createAddressSend(access_token:String! input:createAddressSendInput!): AddressSend
      updateAddressSend(access_token:String!  uuid_address_send:ID! input:updateAddressSendInput!): AddressSend
      delAddressSend(access_token:String!  uuid_address_send:ID!):Boolean
   }

`;
